import Vue from 'vue'
import axios from 'axios'
import cookie from 'vue-cookie'

axios.defaults.baseURL = 'api/'

axios.interceptors.request.use((d) => {
  const tok = cookie.get('login_token')
  d.headers['X-Authorization'] = tok
  // console.log(d)
  return d
})

axios.interceptors.response.use((d) => {
  return d.data
})

Vue.prototype.$http = axios

export default axios
